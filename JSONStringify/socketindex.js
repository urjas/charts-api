var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http, {
    transports: ['polling', 'websocket'],
});
var path = require('path');
var fileUpload = require('express-fileupload');
const sqlite3 = require('sqlite3').verbose();
var obj={};
var obj2={};
var data={};
var data2={};
var dbfile;
var sqlquery2;
var db;
var type2;
var direction2;
var arr=new Array()

io.attach(4567);

io.on('connection', function(socket){
/*  socket.on('formdata', function(msg){
  	console.log(msg);
      io.emit('datarec',msg);
  });*/
  console.log('connected with unity');
  socket.on('hover',function(hoverData){
  	console.log(hoverData.Hover);
  	console.log(sqlquery2);
  	arr=[];
  	
  		db.each(sqlquery2, [hoverData.Hover], (err, rows) => {
  			if (err) {
    			return console.error(err.message);
  			}
 			obj['type']=type2;
  			obj['direction']=direction2;
 			var temp={};
	 		temp['name'] = `${rows.field1}`;
	 		temp['value'] = parseInt(`${rows.field2}`);
	 		arr.push(temp);
	 		console.log(JSON.stringify(arr) + " arr inside");
		},(err,rows)=> {
			console.log(JSON.stringify(arr) + " outside");
			data['Data'] = arr;
			obj['file'] =JSON.stringify(data);
			console.log(JSON.stringify(obj));
			io.emit('datarec',obj);
		});
  		//console.log(JSON.stringify(arr2) + " outside");
  	

  });

});

app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());


app.post('/upload',function(req,res){

	if(!req.files) return res.status(400).send('no files were uploaded');
	obj['type']=req.body.type;
	obj['direction']=req.body.direction;
	type2=req.body.type2;
	direction2=req.body.direction2;
	//obj['sqlquery1']=req.body.sqlquery1;
	dbfile = req.files.dbfile;
	dbfile.mv(dbfile.name ,function(err){
		if(err) return res.status(500).send(err);
		res.send('file uploaded ');
	});

	db = new sqlite3.Database(dbfile.name,(err)=> {
		if(err) {console.err(err.message);}
		console.log(dbfile.name + 'connected');
	});
	let sqlquery1=req.body.sqlquery1;
	sqlquery2=req.body.sqlquery2;
	//console.log(sqlquery2);
	db.all(sqlquery1,[],(err,rows)=>{
		if(err) 
			throw err;
		rows.forEach((row) => {
			var temp={};
			temp['name'] = row.field1;
			temp['value'] = row.field2;
			arr.push(temp);
		});
		console.log(arr);
		data['Data'] = arr;
		obj['file'] =JSON.stringify(data);
		console.log(JSON.stringify(obj));

		io.emit('datarec',obj);
	});
	//db.close();
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/index.js', function(req, res){
  res.sendFile(__dirname + '/index.js');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});