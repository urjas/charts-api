﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using LitJson;
using System.IO;
using UnityEngine.UI;

public class PieGraphFeed : MonoBehaviour {

	private string jsonString;
	private JsonData hmData;
	private PieChart pieChart;
	private string[] hmName;
	private int[] hmValue;
	public Material mat1,mat2;
	public bool ismat1;

	void Awake()
	{
		
		pieChart = GetComponent<PieChart> ();
//		for (int i = 0; i < 22; i++) 
//		{
//			cat [i] = pieChart.DataSource.GetCategoryName (i);
//		}
	}

	void Start()
	{
		//		jsonString = File.ReadAllText (Application.dataPath + "/Datas/piestahome.json");
		jsonString = File.ReadAllText ("data.json");
		print (jsonString);
		hmData = JsonMapper.ToObject (jsonString);
		ismat1 = true;
		if (pieChart != null) 
		{
			hmName = new string[hmData ["Data"].Count];
			hmValue = new int[hmData ["Data"].Count];
			GetData("Data");
			SetData ("Data");
		}

	}

	JsonData GetData(string type)
	{
		for (int i = 0; i < hmData [type].Count; i++) 
		{
			JsonData n = hmData ["Data"] [i] ["name"];
			hmName [i] = n.ToString ();

			JsonData v = hmData ["Data"] [i] ["value"];
			hmValue [i] = (int)v;

		}

		return null;
	}

	public void SetData(string type)
	{
		for (int i = 0; i < hmData [type].Count; i++) 
		{
			//pieChart.DataSource.RenameCategory (cat [i], hmName [i]); 
			if (ismat1) {
				pieChart.DataSource.AddCategory (hmName [i], mat1);
				ismat1 = false;
			} else {
				pieChart.DataSource.AddCategory (hmName [i], mat2);
				ismat1 = true;
			}
			pieChart.DataSource.SetValue (hmName [i],hmValue [i]);
		}
	}


}
