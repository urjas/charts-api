﻿#region License
/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System.Collections;
using UnityEngine;
using SocketIO;
using UnityEngine.SceneManagement;
using ChartAndGraph;

public class TestHover : MonoBehaviour
{
	
	public string categorySelected,categoryPrevious;
	//private JSONObject hoverData;
	public bool gazing;
	public float timer;
	public void Start() 
	{
		
		//hoverData = new JSONObject (JSONObject.Type.STRING);
		//gazing = false;
		categorySelected = "";
		timer = 0.0f;
	}




//	public void BarHovered(BarChart.BarEventArgs args){
//		categorySelected = args.Category;
//		gazing = true;
//
//	}
	public void PieHovered(PieChart.PieEventArgs args){
		print (args.Category);
		categorySelected = args.Category;
		gazing = true;
		print (categorySelected + " hovering " + gazing + " " + timer );
	}
//	public void PointHovered(GraphChart.GraphEventArgs args){
//		categorySelected = args.Category;
//		gazing = true;
//	}
	public void NonHovered() {
		print ("nonhver");
		//categorySelected = null;
		gazing = false;
		timer = 0.0f;
	}
	void Update() {
		print (gazing + " " + categorySelected + " " + timer);
		if(gazing){
			if (categorySelected != categoryPrevious) {
				categoryPrevious = categorySelected;
				timer = 0.0f;
			} else {
				print (timer);
				timer += Time.deltaTime;
			}
			if(timer > 2.0f){
				print (categorySelected);
				//hoverData.AddField ("Hover", categoryPrevious);
				//socket.Emit ("hover",hoverData);
				gazing = false;


			}
		}
	}



}
