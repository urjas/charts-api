﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using LitJson;
using System.IO;

public class BarGraphFeed : MonoBehaviour {

	private string jsonString;
	private JsonData psData;
	private BarChart barchart;
	public string[] psName;
	public int[] psValue;
	public Material mat;
	void Awake()
	{
		barchart = GetComponent<BarChart> ();
//		for (int i = 0; i < 5; i++) 
//		{
//			cat [i] = barchart.DataSource.GetCategoryName (i);
//		}
	}

	void Start () {
		//		jsonString = File.ReadAllText (Application.dataPath + "/Datas/datedata.json");
		jsonString = File.ReadAllText ("data.json");
		psData = JsonMapper.ToObject (jsonString);
		barchart = GetComponent<BarChart> ();
		if (barchart != null) 
		{
			psName = new string[psData ["Data"].Count];
			psValue=new int[psData ["Data"].Count];
			GetData("Data");
			SetData ("Data"); 
		}
	}

	JsonData GetData(string type)
	{
		for (int i = 0; i < psData [type].Count; i++) 
		{
			JsonData n = psData ["Data"] [i] ["name"];
			psName [i] = n.ToString ();

			JsonData v = psData ["Data"] [i] ["value"];
			psValue [i] = (int)v;

		}
		return null;
	}

	public void SetData(string type)
	{
		barchart.DataSource.AddGroup ("Data");
		for (int i = 0; i < psData [type].Count; i++) 
		{
			barchart.DataSource.AddCategory (psName [i], mat);
			//barchart.DataSource.RenameCategory (cat [i], psName [i]); 
			barchart.DataSource.SetValue (psName [i],"Data",psValue [i]);
		}
	}
}


