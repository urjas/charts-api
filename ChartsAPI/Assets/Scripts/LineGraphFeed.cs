﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using ChartAndGraph;
using System.IO;
using UnityEngine.SceneManagement;


public class LineGraphFeed : MonoBehaviour
{
	private string jsonString;
	private JsonData mn;
	private GraphChartBase graph;
	public int[] psName;
	public int[] psValue;
	private string[] cat;

	void Start ()
	{
		graph = GetComponent<GraphChartBase>();
		jsonString = File.ReadAllText ("data.json");
		print (jsonString);
		mn = JsonMapper.ToObject (jsonString);


		if (graph != null)
		{
			psName = new int[mn ["Data"].Count];
			psValue = new int[mn ["Data"].Count];
			GetData("Data");
			SetData ("Data");

		}
	}



	void GetData(string type)
	{
		for (int i = 0; i < mn [type].Count; i++) 
		{

			JsonData n = mn [type] [i] ["name"];
			psName [i] = (int)n;
			JsonData v = mn [type] [i] ["value"];

			psValue [i] = (int)v;
		}

	}

	void SetData(string type)
	{
		graph.DataSource.StartBatch();
		graph.DataSource.RenameCategory("Player 2", "Data");
		graph.DataSource.ClearCategory("Data");
		for (int i = 0; i <mn [type].Count; i++)
		{
			graph.DataSource.AddPointToCategory("Data",psName[i],psValue[i]);

		}
		graph.DataSource.EndBatch();
	}

	public void PointHovered(GraphChart.GraphEventArgs args) {
		print (args.XString);
	}

}

