﻿#region License
/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System.Collections;
using UnityEngine;
using SocketIO;
using UnityEngine.SceneManagement;
using ChartAndGraph;

public class TestSocketIO : MonoBehaviour
{
	private SocketIOComponent socket;
	public string type;
	public string direction;
	public string data;
	private Vector3 position;
	public Transform bar,pie,line;
	private Quaternion rot;
	public string description1, description2;
	public string categorySelected,categoryPrevious;
	private JSONObject hoverData;
	public bool gazing;
	public float timer;
	public void Start() 
	{
		rot = Quaternion.identity;
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();
		socket.On("open", TestOpen);
		socket.On("datarec", DataRec);
		socket.On("close", TestClose);
		hoverData = new JSONObject (JSONObject.Type.STRING);
		categorySelected = "";
		timer = 0.0f;
	}


	public void TestOpen(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
		Debug.Log ("connection opened");
		//socket.Emit ("test");

	}
	
	public void DataRec(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] datarec received: "+ e.data);

		if (e.data == null) { return; }
//		GameObject previous = GameObject.FindGameObjectWithTag ("Graph");
//		if (previous != null) {
//			Destroy (previous);
//		}
		type = e.data.GetField("type").str;
		print (e.data.GetField("type").str);
		print (e.data.GetField("file").str);
		direction = e.data.GetField ("direction").str;
		//description1 = e.data.GetField ("description1").str;
		data = e.data.GetField ("file").str.Replace("\\","");
		print (data);
		System.IO.File.WriteAllText ("data.json", data);
		DetermineDirection ();
		InstantiateGraph ();
	}

	public void BarHovered(BarChart.BarEventArgs args){
		categorySelected = args.Category;
		gazing = true;

	}
	public void PieHovered(PieChart.PieEventArgs args){
		categorySelected = args.Category;
		gazing = true;
		print (categorySelected + " hovering " + gazing );
	}
	public void PointHovered(GraphChart.GraphEventArgs args){
		categorySelected = args.XString;
		gazing = true;
	}
	public void NonHovered() {
		print ("nonhver");
		//categorySelected = null;
		gazing = false;
		timer = 0.0f;
	}
	void Update() {
		print (gazing + " " + categorySelected + " " + timer);
		if(gazing){
			if (categorySelected != categoryPrevious) {
				categoryPrevious = categorySelected;
				timer = 0.0f;
			} else {
				print (timer);
				timer += Time.deltaTime;
			}
			if(timer > 2.0f){
				print (categorySelected);
				hoverData.AddField ("Hover", categoryPrevious);
				socket.Emit ("hover",hoverData);
				gazing = false;


			}
		}
	}
	private void DetermineDirection(){
		if (direction.Equals ("left")) {
			position = new Vector3 (-15.0f,0f,0f);
			rot.eulerAngles = new Vector3 (0.0f,90.0f,0.0f);
		}
		else if (direction.Equals ("right")) {
			position = new Vector3 (15.0f,0f,0f);
			rot.eulerAngles = new Vector3 (0.0f,90.0f,0.0f);
		}
		else if (direction.Equals ("front")) {
			position = new Vector3 (0.0f,0f,15.0f);
			rot.eulerAngles = new Vector3 (0.0f,0.0f,0.0f);

		}
		else if (direction.Equals ("back")) {
			position = new Vector3 (0.0f,0f,-15.0f);
			rot.eulerAngles = new Vector3 (0.0f,0.0f,0.0f);

		}
	}

	private void InstantiateGraph(){
		if (type.Equals ("bar")) {
			Instantiate (bar, position, rot);
			GameObject bart = GameObject.FindWithTag("Graph");
			WorldSpaceBarChart barch = bart.GetComponent<WorldSpaceBarChart> ();
			barch.BarHovered.AddListener (BarHovered);
		}
		if (type.Equals ("pie")) {
			//print ("pie selected");
			Instantiate (pie, position, rot);
			GameObject piet = GameObject.FindWithTag("Graph");
			WorldSpacePieChart piech = piet.GetComponent<WorldSpacePieChart> ();
			piech.PieHovered.AddListener (PieHovered);
		}
		if (type.Equals ("line")) {
			Instantiate (line, position, rot);
			GameObject linet = GameObject.FindWithTag("Graph");
			WorldSpaceGraphChart linech = linet.GetComponent<WorldSpaceGraphChart> ();
			linech.PointHovered.AddListener (PointHovered);
		}
	}
	public void TestError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}
	
	public void TestClose(SocketIOEvent e)
	{	
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}


}
